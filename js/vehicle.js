const Vehicle = (x, y) => new obVehicle(x, y);

function obVehicle(x, y){
	this.radius = 6;
	this.color = map(x, 0, width, 120, 360);
	this.maxSpeed = 5;
	this.maxForce = 0.3;
	
	this.pos = createVector(x, y);
	this.vel = createVector(random(-1, 1), random(-1, 1));
	this.acl = createVector(0.0, 0.0);
	this.target = createVector(x, y);
	
	this.update = function(){
		this.pos.add(this.vel);
		this.vel.add(this.acl);
		
		this.vel.limit(3);
		this.acl.mult(0); 
	};
	
	this.behaviors = function(mouseDown){
		let steer = this.arrive(this.target);
		
		let mouse = createVector(mouseX, mouseY);
		let flee = this.flee(mouse);
		
		steer.mult(2);
		flee.mult(5);
		
		if (!mouseDown) this.applyForce(steer);
		this.applyForce(flee);
	};
	
	this.seek = function(trg){
		let desired = trg.copy().sub(this.pos);
		let d = desired.mag();
		
		desired.setMag(this.maxSpeed);
		
		let steer = desired.sub(this.vel);
		steer.limit(this.maxForce);
		
		return steer;
	};
	this.arrive = function(trg){
		let desired = trg.copy().sub(this.pos);
		let d = desired.mag();
		let speed = this.maxSpeed;
		
		if (d < 25){
			speed = map(d, 0, 25, 0, this.maxSpeed);
		}
		
		desired.setMag(speed);
		
		let steer = desired.sub(this.vel);
		steer.limit(this.maxForce);
		
		return steer;
	};
	this.flee = function(trg){
		let desired = trg.copy().sub(this.pos);		
		let d = desired.mag();
		
		if (d < 80){
//			desired.setMag(this.maxSpeed);
			desired.mult(-1);

			let steer = desired.sub(this.vel);
//			steer.limit(this.applyForce);

			return steer;
		}
		
		return createVector(0, 0);
	};
	
	this.applyForce = function(force){
		this.acl.add(force);
	};
	
	this.show = function(){		
		stroke(this.color, 255, 255);
		strokeWeight(this.radius);
		point(this.pos.x, this.pos.y);
		
		if (this.pos.x < 0){
			this.pos.x = 0;
			this.vel.x *= -1;
		}
		else if (this.pos.x > width){
			this.pos.x = width;
			this.vel.x *= -1;
		}
		if (this.pos.y < 0){
			this.pos.y = 0;
			this.vel.y *= -1;
		}
		else if (this.pos.y > height){
			this.pos.y = height;
			this.vel.y *= -1;
		}
	};
}
